import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html'
})
export class IntroPage {

  slides = [
    {
      title: "DogMeet",
      description: "Dogs Meet Dogs, People Meet Dogs, People Meet People",
      image: "./assets/Picture1.png",
      color: "#9ae8e1"
    },
    {
      title: "Find dog friends!",
      description: "You can find dog and human friends!",
      image: "./assets/Picture1.png",
      color: "#9ae8e1"
    },
    {
      title: "Schedule Meet Ups",
      description: "Easily manage and sign-up to meet",
      image: "./assets/Picture1.png",
      color: "#9ae8e1"
    },
    {
      title: "Stay in the know",
      description: "get updates about dogs you love!",
      image: "./assets/Picture1.png",
      color: "#9ae8e1"
    }
  ];


  constructor(public navCtrl: NavController) {
  }

}
