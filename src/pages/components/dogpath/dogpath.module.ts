import { NgModule } from '@angular/core';
import { DogPathPage } from './dogpath';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    DogPathPage,
  ],
  imports: [
    IonicPageModule.forChild(DogPathPage),
  ],
  exports: [
    DogPathPage
  ]
})
export class DogPathModule {}
