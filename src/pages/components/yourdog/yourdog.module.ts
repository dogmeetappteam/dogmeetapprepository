import { NgModule } from '@angular/core';
import { YourDogPage } from './yourdog';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    YourDogPage,
  ],
  imports: [
    IonicPageModule.forChild(YourDogPage),
  ],
  exports: [
    YourDogPage
  ]
})
export class YourDogModule {}
