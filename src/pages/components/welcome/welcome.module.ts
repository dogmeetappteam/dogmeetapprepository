import { NgModule } from '@angular/core';
import { WelcomePage } from './welcome';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    WelcomePage,
  ],
  imports: [
    IonicPageModule.forChild(WelcomePage),
  ],
  exports: [
    WelcomePage
  ]
})
export class WelcomeModule {}
