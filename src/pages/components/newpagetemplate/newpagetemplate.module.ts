import { NgModule } from '@angular/core';
import { NewPageTemplatePage } from './newpagetemplate';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    NewPageTemplatePage,
  ],
  imports: [
    IonicPageModule.forChild(NewPageTemplatePage),
  ],
  exports: [
    NewPageTemplatePage
  ]
})
export class NewPageTemplateModule {}
