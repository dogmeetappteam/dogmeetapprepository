import { NgModule } from '@angular/core';
import { MultisearchPage } from './multisearch';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    MultisearchPage,
  ],
  imports: [
    IonicPageModule.forChild(MultisearchPage),
  ],
  exports: [
    MultisearchPage
  ]
})
export class MultisearchPageModule {}
