import { Component, OnInit } from '@angular/core';
import { IonicPage, ModalController, LoadingController  } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import 'rxjs/add/operator/map'; // you might need to import this, or not depends on your setup
import * as _ from 'lodash';


@IonicPage()
@Component({
  selector: 'page-multisearch',
  templateUrl: 'multisearch.html'
})
export class MultisearchPage implements OnInit {
    contact: FirebaseListObservable<any[]>;
    contactArray : any=[];
    contactList : any=[]; // store firebase data to local array
    loadedContactList:  any=[];
    filteredDogs: any;


    /// filter -able properties
    gender: string;
    age: Array<string>;
    size: Array<string>;
    shelter: Array<string>;
    name: Array<string>;
    friendliness: Array<string>;
    type: Array<string>;

/// Active filter rules
    filters = {}
    constructor( public loadingCtrl: LoadingController, public afDB: AngularFireDatabase  ) {

      let loadingPopup = this.loadingCtrl.create({
        spinner: 'crescent', // icon style //
        content: ''
      });
      loadingPopup.present();
      this.contact = afDB.list('/contact');
      this.contact.subscribe(contact => {
            this.contactArray = contact;
            this.contactList = this.contactArray; // for ngFor loop
            this.loadedContactList = this.contactArray;
            loadingPopup.dismiss()
      })
      ngOnInit() {
        this.contact = afDB.list('/contact');
        this.contact.subscribe(contact => {
        this.contactArray = contact;
        this.applyFilters()
      }
      }
  }

public applyFileters() {
  this .filteredDogs = _.filter(this.contact, _.conforms(this.filters))
}

//filter property by equalit to rule
filterExact(property: string, rule: any) {
  this.filters[property] = val => val == rules
  this.applyFilters()
}

  initializeItems(){
    this.contactList = this.loadedContactList;
  }

  getItems(searchbar) {
    // Reset items back to all of the items
    this.initializeItems();
    // set q to the value of the searchbar
    var q = searchbar.srcElement.value;
    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }
    this.contactList = this.contactList.filter((v) => {
      if(v.shelter && q) {
        if (v.shelter.toLowerCase().indexOf(q.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });

    console.log(q, this.contactList.length);

  }

}
